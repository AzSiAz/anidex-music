// @flow
import got from 'got'
import cheerio from 'cheerio'

const indexPage = 'https://music.anidex.moe'
const generateAlphabetAlbumsPage = letter => 
    `https://music.anidex.moe/ajax/page.ajax.php?page=albums&alpha_sort=${letter.toUpperCase()}`
const generateAlbumsPageById = id =>
    `https://music.anidex.moe/ajax/page.ajax.php?page=album&id=${id}`
const defaultOptionGot = {
    useElectronNet: false,
    headers: {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36',
        'accept': 'text/html, */*; q=0.01',
        'referer': 'https://music.anidex.moe/',
        'authority': 'music.anidex.moe',
        'x-requested-with': 'XMLHttpRequest'
    }
}

function getAlphabetLink(data) {
    const $ = cheerio.load(data)
    const alphabets = []

    $('li.albums').each((i, el) => 
        alphabets.push(generateAlphabetAlbumsPage($(el).attr('id')))
    )

    return alphabets
}

function getAlphabetAlbumLink(data) {
    const $ = cheerio.load(data)
    const albumsLink = []

    $('a.album_art').each((i, el) =>
        albumsLink.push(generateAlbumsPageById($(el).attr('id')))
    )

    return albumsLink
}

function getAlbumData(data) {
    const $ = cheerio.load(data)
    
    const tracks = ($) => {
        const tracks = []
        
        $('div#collapseOne > table > tbody > tr[title="Add track"]').each((i, el) => {
            // 01 - Believe x Believehttps://music.anidex.moe/music/[Nipponsei] Absolute Duo ED Single - Believe x Believe [Various]/01 - Believe x Believe.mp3 
            function parseScript(str) {
                const t = str
                    .trim()
                    .replace("add_to_playlist('", '')
                    .replace("');", '')
                    .trim()
                    .split("', '")
                return {
                    title: t[0],
                    link: t[1]
                }
            }

            const { link, title } = parseScript($(el).attr('onclick'))

            tracks.push({
                title,
                link,
                size: $(el).find('td:nth-child(2)').first().text()
            })
        })

        return tracks
    }
    const imageScans = ($) => {
        const scans = []

        $('div.frame-square > a').each((i, el) => 
            scans.push({
                url: `https://music.anidex.moe${$(el).attr('href')}`,
                title: $(el).attr('data-title')
            })
        )

        return scans
    }
    const directory = ($) => $('div.col-sm-4 > a > img')
                                .attr('src')
                                .replace('/music/', '')
                                .replace('/cover.jpg', '')

    return {
        title: $('div.col-sm-4 > a > img').attr('title'),
        cover: 
            `https://music.anidex.moe${$('div.col-sm-4 > a > img').attr('src')}`,
        tracks: tracks($),
        imageScans: imageScans($),
        directory: directory($)
    }
}

async function main() {
    console.log('Welcome to anidex-music scrapper')
    console.log('Starting')

    const mainPage = await got('https://music.anidex.moe', defaultOptionGot)

    const alphabets = getAlphabetLink(mainPage.body)
    let albumsLink = []

    console.log('Aggregating albums link')
    for(const alphabet of alphabets) {
        const { body } = await got(alphabet, defaultOptionGot)
        albumsLink = [...albumsLink, ...getAlphabetAlbumLink(body)]
    }

    console.log(`Found ${albumsLink.length - 1} link`)
    for(const albumLink of albumsLink) {
        console.log(`Getting ${albumLink}`)

        const { body } = await got(albumLink, defaultOptionGot)
        const album = getAlbumData(body)

        console.log(`Got ${album.title}`)
    }
    console.log('Done')
}

main().catch(err => {
    console.error(err)
})
